﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DebagleWebSiteTest.PageObject
{
    class MyAccauntPajeObject
    {
        private IWebDriver webdriver;
        private readonly By _myAccauntDropDownButton = By.CssSelector("#trc > div > ul.user-menu.show-for-medium-up > li > span");
        private readonly By _accauntSettings = By.CssSelector("#trc > div > ul.user-submenu > li:nth-child(3) > a");
        private readonly By _firstNameInputButton = By.Name("data[User][first_name]");
        private readonly By _lastNameInputButton = By.Name("data[User][last_name]");
        private readonly By _emailInputButton = By.Name("data[User][email]");
        private readonly By _timeZoneButton = By.Name("data[User][timezone]");
        private readonly By _saveChangeseButton = By.CssSelector("#UserSettingsForm > div.submit > input");
        private readonly By _canselAccauntButton = By.CssSelector("#UserSettingsForm > div.submit > a");
        private readonly By _reasonofCanselDropDown = By.Name("data[UserCancelation][reason]");
        private readonly By _commentOfRemovingAccaunt = By.Name("data[UserCancelation][comment]");
        private readonly By _finishCanselAccauntButton = By.Name("#UserCancelationCancelForm > div.submit > input");
        private readonly By _changePasswordBlock = By.CssSelector("#my-account > ul > li:nth-child(2) > i");
        private readonly By _oldPasswordInputButton = By.Name("data[User][old_password]");
        private readonly By _newPasswordInputButton = By.Name("data[User][register_password]");
        private readonly By _newPasswordConfirmInputButton = By.Name("data[User][register_password_confirm]");
        private readonly By _changePasswordButton = By.CssSelector("#UserSettingsForm > div.submit > input");






        public MyAccauntPajeObject(IWebDriver webdriver)
        {
            this.webdriver = webdriver;
        }
        public string GetUrl() => webdriver.Url;
        IWebElement GetAccaunDropDown() => webdriver.FindElement(_myAccauntDropDownButton);
        IWebElement GetAccaunSettings() => webdriver.FindElement(_accauntSettings);
        IWebElement GetFirstName() => webdriver.FindElement(_firstNameInputButton);
        IWebElement GetLastName() => webdriver.FindElement(_lastNameInputButton);
        IWebElement GetEmail() => webdriver.FindElement(_emailInputButton);
        IWebElement GettimeZoneDropDown() => webdriver.FindElement(_timeZoneButton);
        IWebElement GetSaveChangesButton() => webdriver.FindElement(_saveChangeseButton);
        IWebElement GetCanselAccauntButton() => webdriver.FindElement(_canselAccauntButton);
        IWebElement GetReasonOfRemove() => webdriver.FindElement(_reasonofCanselDropDown);
        IWebElement GetCommentOfRemoving() => webdriver.FindElement(_commentOfRemovingAccaunt);
        IWebElement GetFinishRemoving() => webdriver.FindElement(_finishCanselAccauntButton);
        IWebElement GetPasswordBlock() => webdriver.FindElement(_changePasswordBlock);
        IWebElement GetOldPassword() => webdriver.FindElement(_oldPasswordInputButton);
        IWebElement GetNewPassword() => webdriver.FindElement(_newPasswordInputButton);
        IWebElement GetNewPasswordConfirm() => webdriver.FindElement(_newPasswordConfirmInputButton);
        IWebElement GetChangePasswordm() => webdriver.FindElement(_changePasswordButton);
        public IWebElement LogOutButton() => webdriver.FindElement(By.CssSelector("#trc > div > ul.user-submenu > li:nth-child(5) > a"));

        public string GetActualFirstName() => webdriver.FindElement(_firstNameInputButton).GetAttribute("value");
        public string GetActualLasttName() => webdriver.FindElement(_lastNameInputButton).GetAttribute("value");
        public string GetActualEmail() => webdriver.FindElement(_emailInputButton).GetAttribute("value");
        public string GetActualTimeZone() => webdriver.FindElement(By.XPath("//select[@name='data[User][timezone]']//option[@selected]")).Text;
        public string GetCanselMessege() => webdriver.FindElement(By.XPath("/html/body/div/div/div[2]/div/text()")).Text;









        public MyAccauntPajeObject ChangePersonalData(string newname,string newlastname,string newemail)
        {
            var dropdown = GetAccaunDropDown();
            dropdown.Click();
            var settings = GetAccaunSettings();
            settings.Click();

            var firstname = GetFirstName();
            firstname.Clear();
            firstname.SendKeys(newname);
            var lastname = GetLastName();
            lastname.Clear();
            lastname.SendKeys(newlastname);
            var email = GetEmail();
            email.Clear();
            email.SendKeys(newemail);
            var timeZone = GettimeZoneDropDown();
            timeZone.Click();
            timeZone.SendKeys(Keys.ArrowDown);
            timeZone.SendKeys(Keys.ArrowDown);
            timeZone.SendKeys(Keys.Enter);
            var savebutt = GetSaveChangesButton();
            savebutt.Click();

            return new MyAccauntPajeObject(webdriver);
        }
        public MyAccauntPajeObject RemoveAccaunt(string commenttoremove)
        {
            var dropdown = GetAccaunDropDown();
            dropdown.Click();
            var settings = GetAccaunSettings();
            settings.Click();

            var canselAccauntButton = GetCanselAccauntButton();
            canselAccauntButton.Click();
            var reasonOfCansel = GetCanselAccauntButton();
            reasonOfCansel.Click();
            reasonOfCansel.SendKeys(Keys.ArrowDown);
            reasonOfCansel.SendKeys(Keys.Enter);
            var comment = GetCommentOfRemoving();
            comment.SendKeys(commenttoremove);

            var cansel = GetFinishRemoving();
            cansel.Click();

            return new MyAccauntPajeObject(webdriver);
        }
        public MyAccauntPajeObject ChangePassword(string oldpass,string newpass,string passconfirm)
        {
            var dropdown = GetAccaunDropDown();
            dropdown.Click();
            var settings = GetAccaunSettings();
            settings.Click();
            var passblock = GetPasswordBlock();
            passblock.Click();
            var oldpassword = GetOldPassword();
            oldpassword.SendKeys(oldpass);
            var newpassword = GetNewPassword();
            newpassword.SendKeys(newpass);
            var confirm = GetNewPasswordConfirm();
            confirm.SendKeys(passconfirm);
            confirm.SendKeys(Keys.Enter);

            return new MyAccauntPajeObject(webdriver);
        }
        public SignInPageObject LogOut()
        {
            var dropdown = GetAccaunDropDown();
            dropdown.Click();
            var logout = LogOutButton();
            logout.Click(); 

            return new SignInPageObject(webdriver);
        }








    }
}
