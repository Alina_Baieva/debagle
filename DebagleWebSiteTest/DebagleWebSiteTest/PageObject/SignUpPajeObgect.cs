﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DebagleWebSiteTest.PageObject
{
    public class SignUpPajeObgect
    {
        private IWebDriver webdriver;
        private readonly By _firstNameInputButton = By.Name("data[User][first_name]");
        private readonly By _lastNameInputButton = By.Name("data[User][last_name]");
        private readonly By _timeZoneButton = By.Name("data[User][timezone]");
        private readonly By _emailInputButton = By.Name("data[User][email]");
        private readonly By _passwordnputButton = By.Name("data[User][register_password]");
        private readonly By _passwordConfirmnputButton = By.Name("data[User][register_password_confirm]");
        private readonly By _finishSignUpButton = By.XPath("//div[@class ='submit']//input[@class ='button']");
        private readonly By _logInButton = By.CssSelector("body > div.row > div > div.container > p > a");

        public SignUpPajeObgect(IWebDriver webdriver)
        {
            this.webdriver = webdriver;
        }
        IWebElement GetFirstName() => webdriver.FindElement(_firstNameInputButton);
        IWebElement GetLastName() => webdriver.FindElement(_lastNameInputButton);
        IWebElement GetTimeZone() => webdriver.FindElement(_timeZoneButton);
        IWebElement GetEmail() => webdriver.FindElement(_emailInputButton);
        IWebElement GetPassword() => webdriver.FindElement(_passwordnputButton);
        IWebElement GetPasswordConfirm() => webdriver.FindElement(_passwordConfirmnputButton);
        IWebElement GetFinishSignUpButton() => webdriver.FindElement(_finishSignUpButton);
        IWebElement GetLogInButton() => webdriver.FindElement(_logInButton);
        public string GetEmailError() => webdriver.FindElement(By.XPath("//*[@id='UserSignupForm']/div[4]/div/div/div")).Text;
        public string GetPasswordError() => webdriver.FindElement(By.XPath("//*[@id='UserSignupForm']/div[5]/div/div/div")).Text;
        public string GetFirstNameError() => webdriver.FindElement(By.XPath("//*[@id='UserSignupForm']/div[2]/div[1]/div/div")).Text;
        public string GetLastNameError() => webdriver.FindElement(By.XPath("//*[@id='UserSignupForm']/div[2]/div[2]/div/div")).Text;
        public string GetPasswordConfirmError() => webdriver.FindElement(By.CssSelector("#UserSignupForm > div:nth-child(6) > div > div > div")).Text;



        public string GetUrl() => webdriver.Url;
        public MainMenuPageObject SignUp(string fname,string lname,string email,string password,string passwordconfirm)
        {
            var firstNameField = GetFirstName();
            firstNameField.Click();
            firstNameField.SendKeys(fname);

            var lastNameField = GetLastName();
            lastNameField.Click();
            lastNameField.SendKeys(lname);

            var timeZone = GetTimeZone();
            timeZone.Click();
            timeZone.SendKeys(Keys.ArrowDown);
            timeZone.SendKeys(Keys.Enter);

            var emailField = GetEmail();
            emailField.Click();
            emailField.SendKeys(email);

            var passwordField = GetPassword();
            passwordField.Click();
            passwordField.SendKeys(password);

            var passwordConfirmField = GetPasswordConfirm();
            passwordConfirmField.Click();
            passwordConfirmField.SendKeys(passwordconfirm);

            var signupButton = GetFinishSignUpButton();
            signupButton.Click();
            return new MainMenuPageObject(webdriver);
        }
        public SignInPageObject GoToLogInPage()
        {
            var logInButton = GetLogInButton();
            logInButton.Click();
            return new SignInPageObject(webdriver);
        }

    }
}
