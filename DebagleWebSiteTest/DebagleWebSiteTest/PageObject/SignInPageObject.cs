﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DebagleWebSiteTest.PageObject
{
    public class SignInPageObject
    {

        private IWebDriver webdriver;
        private readonly By _emailInputButton = By.Name("data[User][email]");
        private readonly By _passwordInputButton = By.Name("data[User][password]");
        private readonly By _logInButton = By.XPath("//div[@class = 'submit']//input[@class = 'button']");
        private readonly By _signUpButton = By.CssSelector("#UserLoginForm > div:nth-child(4) > div.row > div > p > small > a:nth-child(2)");


        public SignInPageObject(IWebDriver webdriver)
        {
            this.webdriver = webdriver;
        }
        public string GetUrl() => webdriver.Url;
        IWebElement GetEmail() => webdriver.FindElement(_emailInputButton);
        IWebElement GetPassword() => webdriver.FindElement(_passwordInputButton);
        IWebElement GetLogInButton() => webdriver.FindElement(_logInButton);
        IWebElement GetSignUpButton() => webdriver.FindElement(_signUpButton);
        public string GetAuthorizationError() => webdriver.FindElement(By.CssSelector("#flashMessage")).Text;
        public MainMenuPageObject LogIn(string email,string password)
        {
            var emailField = GetEmail();
            emailField.Click();
            emailField.SendKeys(email);

            var passwordField = GetPassword();
            passwordField.Click();
            passwordField.SendKeys(password);

            var logInButton = GetLogInButton();
            logInButton.Click();
            return new MainMenuPageObject(webdriver);
        }
        public MainMenuPageObject LogInWithNewPassword(string email, string password)
        {
            var emailField = GetEmail();
            emailField.Clear();
            emailField.Click();
            emailField.SendKeys(email);

            var passwordField = GetPassword();
            passwordField.Click();
            passwordField.Clear();
            passwordField.SendKeys(password);

            var logInButton = GetLogInButton();
            logInButton.Click();
            return new MainMenuPageObject(webdriver);
        }
        public SignUpPajeObgect GoToSignUpPage()
        {
            var signUpButton = GetSignUpButton();
            signUpButton.Click();
            return new SignUpPajeObgect(webdriver);
        }

    }
}
