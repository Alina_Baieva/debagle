﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DebagleWebSiteTest.PageObject
{
    public class StartPageObgect
    {
        private IWebDriver webdriver;
        private readonly By _signUpButton = By.CssSelector("#hero > div > a");
        private readonly By _signInButton = By.CssSelector("#hero > div > nav > ul > li > a");
        public StartPageObgect(IWebDriver webdriver)
        {
            this.webdriver = webdriver;
        }
        public IWebElement GetSignUpButton() => webdriver.FindElement(_signUpButton);
        public IWebElement GetSignInButton() => webdriver.FindElement(_signInButton);
    }
}
