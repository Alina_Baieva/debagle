﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DebagleWebSiteTest.PageObject
{
    class ProjectPageObject
    {
        private IWebDriver webdriver;
        private readonly By _newItemInputButton = By.XPath("//input[@name = 'issue-name']");
        private readonly By _choisedate = By.CssSelector("#ui-datepicker-div > table > tbody > tr:nth-child(3) > td:nth-child(1) > a");
        private readonly By _dataPikerButton = By.Id("datepicker-add");
        private readonly By _assigneeButton = By.Id("assignee-id");
        private readonly By _saveNewItemButton = By.XPath("//button[@class= 'button small']");
        private readonly By _setUpButton = By.XPath("//div[@id='open-issues']//div[@class='draggable-handle ui-draggable']");
        private readonly By _markAsImportantButton = By.CssSelector("#actions-bar > ul.secondary-actions > li");
        private readonly By _settingsButton = By.XPath("//ul[@class = 'project-settings']//li[@class='open-project-settings']//i");
        private readonly By _projectNameInputButton = By.XPath("//div[@class='edit-project-name']//h3[@class='editable']");
        private readonly By _projectNewNameInputButton = By.CssSelector("#edit-project-name");
        private readonly By _deleteprojectButton = By.CssSelector("#tab-general > div:nth-child(3) > div:nth-child(2) > div > div.delete-project-wrapper > p > a");
        private readonly By _deleteconfirmInputButton = By.CssSelector("#project-delete");
        private readonly By _deleteconfirmButton = By.CssSelector("#confirm-project-delete");
        private readonly By _isMarkedAsImportant = By.XPath("//*[@id='issue-374196']/div[3]");
        private readonly By _importantIssueList = By.Id("list-important");





        public ProjectPageObject(IWebDriver webdriver)
        {
            this.webdriver = webdriver;
        }
        public string GetUrl() => webdriver.Url;
        IWebElement GetNewItem() => webdriver.FindElement(_newItemInputButton);
        IWebElement GetChoiseDate() => webdriver.FindElement(_choisedate);
        IWebElement GetDataPiker() => webdriver.FindElement(_dataPikerButton);
        IWebElement GetAsignee() => webdriver.FindElement(_assigneeButton);
        IWebElement GetSaveNewItemButton() => webdriver.FindElement(_saveNewItemButton);
        IWebElement GetSetUpItem() => webdriver.FindElement(_setUpButton);
        IWebElement GetMarkAsImportant() => webdriver.FindElement(_markAsImportantButton);
        IWebElement GetSettings() => webdriver.FindElement(_settingsButton);
        IWebElement GetProjectName() => webdriver.FindElement(_projectNameInputButton);
        IWebElement GetDeleteProject() => webdriver.FindElement(_deleteprojectButton);
        IWebElement GetDeleteConfirm() => webdriver.FindElement(_deleteconfirmButton);
        IWebElement GetDeleteConfirmCode() => webdriver.FindElement(_deleteconfirmInputButton);

        IWebElement GetNewProjectNameField() => webdriver.FindElement(_projectNewNameInputButton);

        public IWebElement GetImportantList() => webdriver.FindElement(_importantIssueList);

        public string GetMarkedAsImportant() => webdriver.FindElement(_isMarkedAsImportant).Text;
        public string GetNewProjectName() => webdriver.FindElement(By.XPath("//div[@class = 'edit-project-name']//h3")).Text;




        public ProjectPageObject CreateItem(string item)
        {
            var newitem = GetNewItem();
            newitem.Click();
            newitem.SendKeys(item);

            var datapiker = GetDataPiker();
            datapiker.Click();
            Thread.Sleep(300);
            var choisedate = GetChoiseDate();
            choisedate.Click();

            var asigneebut = GetAsignee();
            asigneebut.Click();
            asigneebut.SendKeys(Keys.ArrowDown);
            asigneebut.SendKeys(Keys.Enter);

            var savebut = GetSaveNewItemButton();
            savebut.Click();
            return new ProjectPageObject(webdriver);

        }
        public ProjectPageObject MarkItemAsImportant()
        {
            var setup = GetSetUpItem();
            setup.Click();
            var mark = GetMarkAsImportant();
            mark.Click();
            return new ProjectPageObject(webdriver);
        }
        public ProjectPageObject ChangeProjectName(string newprojectname)
        {
            var setup = GetSettings();
            setup.Click();
            var changename = GetProjectName();
            changename.Click();
            var newname = GetNewProjectNameField();
            newname.Clear();
            newname.SendKeys(newprojectname);
            newname.SendKeys(Keys.Enter);
            return new ProjectPageObject(webdriver);
        }
        public ProjectPageObject DeleteProject()
        {
            var setup = GetSettings();
            setup.Click();
            var delete = GetDeleteProject();
            delete.Click();
            var deleteconfirmfield = GetDeleteConfirmCode();
            deleteconfirmfield.SendKeys("DELETE");
            var confirmdelete = GetDeleteConfirm();
            confirmdelete.Click();
            return new ProjectPageObject(webdriver);
        }

    }
}
